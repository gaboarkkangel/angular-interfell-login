**Testin to Iterfell in Angular**


## PLATAFORMA

*la el siguiente test, está realizando en Angular 6, no utiliza ninguna dependencia fuera de las que utiliza el core y la instalacion primaria. Posee dos archivos spect.ts con pruebas unitarias de los dos servicios (login - bookings). Posee un auth para la autentificacion del usuario.*

---

## INSTALACION

Indicaciones para la instalacion.

1. Abra la consola de comandos de su preferencia y ubiquese en el directoria donde desea instalar el source.
2. ingrese para descargar el repositorio: *git clone https://gaboarkkangel@bitbucket.org/gaboarkkangel/angular-interfell-login.git
3. ingrese a la carpeta angular-interfell-login que se acaba de crear **(cd angularinterfell-login)** .
4. Instale las dependencias con el siguiente comando: *npm install.* **NOTA: Tambien puede utilizar yarn** 
5. ejecute la aplicacion en la consola con **ng serve**.

---
