﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class HomeService {
    private apiToken: string;
    private adminEmail: string;
    private bookings: Array<any> = [];

    constructor(private http: HttpClient) { 
        this.apiToken = localStorage.getItem('token');
        this.adminEmail = localStorage.getItem('adminemail');
    }

    public getBookings(email: string, isCurrent?: boolean) {
        let params = new HttpParams();
        params = params.append('current', `${isCurrent}`);
    
        const  OPTIONS = {
          headers: new HttpHeaders({
            'adminemail': this.adminEmail,
            'token': this.apiToken,
            'app': environment.API_APP_NAME
          }),
          params: params
        };
    
        let cleanEmail = email.replace('@', '%40');
    
        return this.http.get(`${environment.API_MAIN_URL}/${environment.API_USER_URL}/${cleanEmail}${environment.API_BOOKING_URL}`, OPTIONS)
          .pipe(
            map((response: Array<any>) => {
              this.bookings = response;
              return response;
            })
          );
      }

      public filterBookings(searchTerm) {
        return this.bookings.filter((booking) => {
          return booking.bookingId.toString().indexOf(searchTerm) > -1;
        });
      }
}
