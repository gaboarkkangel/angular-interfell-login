import { TestBed, inject, async } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('AuthenticationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [AuthenticationService]
    });
  });

  it('should be created auth', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));
  it ('should get login', async(() => {
    const service: AuthenticationService = TestBed.get(AuthenticationService);
    service.login({email : 'testapis@tuten.cl', password : '123'}).subscribe(
      (response) => expect(response.json()).not.toBeNull(),
      (error) => fail(error)
    );
  }));
});
