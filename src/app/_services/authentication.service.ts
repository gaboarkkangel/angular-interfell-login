﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(credentials: Credentials) {

        const options = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'password': credentials.password,
              'app': environment.API_APP_NAME
            })
          };
          const email = credentials.email.replace('@', '%40');


        return this.http.put<any>(`${environment.API_MAIN_URL}${environment.API_USER_URL}/${email}`, null, options)
            .pipe(map(user => {
                console.log(user);
                // login successful if there's a jwt token in the response
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('adminemail', user.email);
                    localStorage.setItem('email', environment.TEST_USER_EMAIL);
                    localStorage.setItem('current', 'true');
                    localStorage.setItem('token', user.sessionTokenBck);
                    localStorage.setItem('app', environment.API_APP_NAME);
                }

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('current');
    }
}
class Credentials {
    public email: string;
    public password: string;
}
