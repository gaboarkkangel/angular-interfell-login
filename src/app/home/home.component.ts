﻿import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HomeService } from '../_services';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    currentUser: any;
    public form: FormGroup;
    public firstSearch: boolean = true;
    public bookings: Array<any> = [];
    public searchTerm: string = '';
    public user: string = localStorage.getItem('email');

    constructor(private formBuilder: FormBuilder,
                private home: HomeService) {
                  this.currentUser = localStorage.getItem('adminemail');
                  this.initForm();
                }

    ngOnInit() {
      this.searchBookings();
    }

    initForm() {
        this.form = this.formBuilder.group({
          email: new FormControl({  value : this.user,
                                    disabled : true } , Validators.required)
        });
    }

    searchBookings() {
        this.home.getBookings(this.user, true)
          .subscribe((response: Array<any>) => {
            console.log(response);
            this.firstSearch = false;
            this.bookings = response;
          }, err => {
            console.log(err);
          });
      }

      setFilteredItems() {
        this.bookings = this.home.filterBookings(this.searchTerm);
      }
}
